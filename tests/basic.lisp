(defpackage :test-fp16-basics
  (:use :cl :lisp-unit))

(in-package "TEST-FP16-BASICS")

(defun fp16-bits (sign exp signif)
  (declare (bit sign)
	   (type (unsigned-byte 5) exp)
	   (type (unsigned-byte 10) signif))
  (logior (ash sign 15)
	  (ash exp 10)
	  signif))

(define-test to-float.zero
    "Test conversion of fp16 to floats"
  (assert-equal 0f0 (fp16:to-float (fp16-bits 0 0 0)))
  (assert-equal -0f0 (fp16:to-float (fp16-bits 1 0 0))))

(define-test to-float.subnormal
    "Test conversion of fp16 subnormal to floats"
  ;; Max subnormal
  (assert-equal (- (scale-float 1f0 -14)
		   (scale-float 1f0 -24))
		(fp16:to-float (fp16-bits 0 0 #b1111111111)))
  ;; Min subnormal
  (assert-equal (scale-float 1f0 -24)
		(fp16:to-float (fp16-bits 0 0 1))))

(define-test to-float.non-finite
    "Test fp16 infinities"
  ;; Positive and negative infinity
  (assert-equal ext:single-float-positive-infinity
		(fp16:to-float (fp16-bits 0 #b11111 0)))
  (assert-equal ext:single-float-negative-infinity
		(fp16:to-float (fp16-bits 1 #b11111 0)))
  ;; NaN
  (assert-true (ext:float-nan-p (fp16:to-float (fp16-bits 0 #b11111 1)))))

(define-test to-float.normal
    "Test fp16 noraml"
  (assert-equal 1f0
		(fp16:to-float (fp16-bits 0 #b01111 0)))
  (assert-equal -1f0
		(fp16:to-float (fp16-bits 1 #b01111 0)))
  (assert-equal -2f0
		(fp16:to-float (fp16-bits 1 #b10000 0)))
  ;; float16-positive-epsilon
  (assert-equal (+ 1 (scale-float 1f0 -10))
		(fp16:to-float (fp16-bits 0 #b01111 1)))
  ;; most-positive-float16
  (assert-equal 65504f0
		(fp16:to-float (fp16-bits 0 #b11110 #b1111111111)))
  ;; most-negative-float16
  (assert-equal -65504f0
		(fp16:to-float (fp16-bits 1 #b11110 #b1111111111))))

(define-test to-fp16.zero
  "Test conversion of float zero to fp16"
  (assert-equal (fp16-bits 0 0 0)
		(fp16:to-fp16 0f0))
  (assert-equal (fp16-bits 1 0 0)
		(fp16:to-fp16 -0f0)))

(define-test to-fp16.non-finite
  "Test conversion of non-finite values to fp16"
  (assert-equal (fp16-bits 0 #b11111 0)
		(fp16:to-fp16 ext:single-float-positive-infinity))
  (assert-equal (fp16-bits 1 #b11111 0)
		(fp16:to-fp16 ext:single-float-negative-infinity))
  (assert-equal (fp16-bits 0 #b11111 #b0100000000)
		(fp16:to-fp16
		 (kernel:make-single-float
		  (logior (ash #xff 23)
			  (ash #b01 21))))))

(define-test to-fp16.normal
  "TEst conversion of normal floats"
  ;; Smaller than the smallest float16
  (assert-equal (fp16-bits 0 0 0)
		(fp16:to-fp16 (scale-float 1f0 -25)))
  (assert-equal (fp16-bits 1 0 0)
		(fp16:to-fp16 (scale-float -1f0 -25)))
  ;; Largest float16
  (assert-equal (fp16-bits 0 #b11110 #b1111111111)
		(fp16:to-fp16 65504f0))
  ;; Overflow
  (assert-equal (fp16-bits 0 #b11111 0)
		(fp16:to-fp16 65536f0))
  (assert-equal (fp16-bits 1 #b11111 0)
		(fp16:to-fp16 -65536f0)))
  