;;; -*- Mode: Lisp -*-

(defsystem "fp16"
  :components
  ((:file "package")
   (:file "fp16" :depends-on ("package"))))
