;; IEEE 754 half-precision format:
;;
;; Sign bit: 1 bit
;; Exponent: 5 bits
;; Significand: 11 bits (10 explicitly stored)
;;
;; Exponent bias is 15.
;;

(in-package "FP16")

(deftype float16 ()
  `(unsigned-byte 16))

(defun to-float (f16)
  (declare (type float16 f16))
  (let ((significand (ldb (byte 10 0) f16))
	(exp (ldb (byte 5 10) f16))
	(sign (ldb (byte 1 15) f16)))
    (cond ((zerop exp)
	   ;; Subnormal numbers because the exponent is 0
	   (* (- 1 (+ sign sign))
	      (scale-float (float significand) (- -14 10))))
	  ((= exp #b11111)
	   ;; Largest exponent value means infinity or NaN
	   (let* ((f32-significand (ash significand (- 23 10)))
		  (bits (logior (ash #b11111111 23)
				f32-significand)))
		 
	     (kernel:make-single-float
	      (dpb bits (byte 31 0) (if (zerop sign) 0 -1)))))
	  (t
	   ;; Convert fp16 exponent to single-precision exponent (subtract
	   ;; fp16 bias and add single-float bias). Convert the fp16
	   ;; significand to single-precision by shifting the value left.
	   (let ((f32-exp (+ (- exp 15) 127))
		 (f32-significand (ash significand (- 23 10))))
	     (kernel:make-single-float
	      (dpb (logior (ash f32-exp 23)
			   f32-significand)
		   (byte 31 0)
		   (* -1 sign))))))))

(defun to-fp16 (f)
  (declare (single-float f))
  ;; We are not currently concerned with numbers that won't fit in a
  ;; float16, or even rounding the single-float to float16.
  (let* ((bits (kernel:single-float-bits f))
	 (significand (ldb (byte 23 0) bits))
	 (exp (ldb (byte 8 23) bits))
	 (sign (ldb (byte 1 31) bits)))
    (cond ((ext:float-nan-p f)
	   ;; Return another NaN
	   (logior
	    (ash sign 15)
	    (ash #b11111 10)
	    (ash significand (- 10 23))))
	  ((< (abs f) (scale-float 1f0 -24))
	   ;; Smallest representable number is 2^-24.  Anything
	   ;; smaller Rounds to 0
	   (ash sign 15))
	  ((< (abs f) (scale-float 1f0 -14))
	   ;; Subnormal numbers)
	   (let ((signif (ldb (byte 10 (- 24 10))
			      (logior (ash 1 23) significand)))
		 (e (- exp 127)))
	     (logior
	      (ash sign 15)
	      (ash 0 10)
	      (ash signif (+ 15 e)))))
	  ((<= (abs f) 65504)
	   ;; Normalized number
	   (logior
	    (ash sign 15)
	    (ash (+ (- exp 127) 15) 10)
	    (ash significand (- 10 23))))
	  (t
	   ;; Everything else. It's either a single-float infinity or
	   ;; larger than the largest float16 value.  Thus, return
	   ;; infinity.
	   (logior (ash sign 15)
		   #b111110000000000)))))

