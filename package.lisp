(defpackage "FP16"
  (:use :cl)
  (:export
   "TO-FLOAT"
   "TO-FP16"))
